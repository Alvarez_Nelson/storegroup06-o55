/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


function loadData(){
     let request = sendRequest('compraproducto/list','GET','');
     let table = document.getElementById('compraproducto-table');
     table.innerHTML = "";
     request.onload = function(){
          let data = request.response;
          console.log(data);
          data.forEach((element)=>{
             table.innerHTML += `
               <tr class="table-warning">
                    <th>${element.idCompra}</th>
                    <td>${element.idProducto.idProducto}
                    ${element.idProducto.nombreProducto}</td>
                    <td>${element.idProveedor.idProveedor}
                    ${element.idProveedor.razonSocial}</td>
                    <th>${element.cantidadProducto}</th>
                    <th>${element.valorCompra}</th>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success"
                    onclick='window.location="FormCompraProducto.html?id=${element.idCompra}"' >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger"
                    onclick='deleteCompra(${element.idCompra})'>Eliminar</button>
                    </td>
               </tr>
               `;
          });
     };
     
     request.onerror = function(){
       table.innerHTML = `
          <tr>
          <td colspan="7">Error al consultar la base de datos la tabla compras</td>
          </tr>
          ` ;  
     };
}

function loadCompra(idCompra){
     let request = sendRequest('compraproducto/list/'+idCompra,'GET','');
     let id = document.getElementById('compraproducto-id');
     let idProducto = document.getElementById('ProductoId');
     let idProveedor = document.getElementById('ProveedorId');
     let cantidadProducto = document.getElementById('Cantidad');
     let valorCompra = document.getElementById('Valor');
     
     request.onload = function(){
          let data = request.response;
          console.log(data);
          id.value = data.idCompra;
          idProducto.value = data.idProducto.idProducto;
          idProveedor.value = data.idProveedor.idProveedor;
          cantidadProducto.value = data.cantidadProducto;
          valorCompra.value = data.valorCompra;

     };
     
     request.onerror = function(){
          alert('Error al consultar los datos de una compra, revise el console log del navegador o el output del IDE tomcat');
     };
}

function deleteCompra(idCompra){
     let request = sendRequest('compraproducto/list/'+idCompra,'DELETE','');
     request.onload = function(){
          loadData();
     };
}

function saveCompraProducto(){
     let idProducto = document.getElementById('ProductoId').value;
     let idProveedor = document.getElementById('ProveedorId').value;
     let cantidadProducto = document.getElementById('Cantidad').value;
     let valorCompra = document.getElementById('Valor').value;
     
     let data ={
          'idProducto':{'idProducto':idProducto},'idProveedor':{'idProveedor':idProveedor},
          'cantidadProducto':cantidadProducto ,'valorCompra':valorCompra         
     };
     
     let request = sendRequest('compraproducto/','POST', data);
     request.onload = function (){
          window.location = 'ComprasProducto.html';
     };
     request.onerror = function(){
          alert('Error al guardar la compra');
     }; 
}

function updateCompraProducto(){
     let id = document.getElementById('compraproducto-id').value;
     let idProducto = document.getElementById('ProductoId').value;
     let idProveedor = document.getElementById('ProveedorId').value;
     let cantidadProducto = document.getElementById('Cantidad').value;
     let valorCompra = document.getElementById('Valor').value;
     
     let data ={
          'idCompra':id,
          'idProducto':{'idProducto':idProducto},'idProveedor':{'idProveedor':idProveedor},
          'cantidadProducto':cantidadProducto ,'valorCompra':valorCompra 
     };
     
     let request = sendRequest('compraproducto/list/'+id,'PUT', data);
     request.onload = function (){
          window.location = 'ComprasProducto.html';
     };
     request.onerror = function(){
          alert('Error al guardar la compra');
     }; 
}