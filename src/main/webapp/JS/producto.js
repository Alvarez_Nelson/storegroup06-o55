function loadData(){
     let request = sendRequest('producto/list','GET','');
     let table = document.getElementById('producto-table');
     table.innerHTML="";
     request.onload= function (){
          let data = request.response;
          console.log(data);
          data.forEach((element,index)=> {
               //en las etiquetas <th>, td, despues del element. son los mismos nombres de las columnas de la base de datos, eje idProducto es como está en la tabla producto
             table.innerHTML += `
               <tr class="table-warning">
                    
                    <th>${element.idProducto}</th>
                    <td>${element.nombreProducto}</td>
                    <td>${element.valorCompra}</td>
                    <td>${element.valorVenta}</td>
                    <td>${element.cantidad}</td>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success" onclick="window.location='FormProducto.html?id=${element.idProducto}'" >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger" onclick='deleteProducto(${element.idProducto})'>Eliminar</button>
                    </td>
               </tr>
               `;
            
          });
     };
     request.onerror = function(){
          table.innerHTML=`
          <tr>
               <td colspan="6">Error al recuperar los datos de productos </td>
          </tr>`;
     };
}

function loadProducto(idProducto){
     let request = sendRequest('producto/list/'+idProducto,'GET','');
     let id = document.getElementById('producto-id');
     let name = document.getElementById('producto-name');
     let purchase = document.getElementById('producto-compra');
     let sale = document.getElementById('producto-venta');
     let quantity = document.getElementById('producto-cantidad');
     
     request.onload = function(){
          let data = request.response;
          //despues del igual lo que acompaña a data son las columnas de la tabla en la base de datos
          id.value= data.idProducto;
          name.value = data.nombreProducto;
          purchase.value= data.valorCompra;
          sale.value = data.valorVenta;
          quantity.value = data.cantidad;
          
     };
     request.onerror = function(){
          alert("Error al consultar el producto");
     };
     
}

function deleteProducto(idProducto){
     let request = sendRequest('producto/list/'+idProducto,'DELETE','');
     request.onload = function(){
          loadData();
     };
}
 
function saveProducto(){
     let name = document.getElementById('producto-name').value;
     let compra = document.getElementById('producto-compra').value;
     let venta = document.getElementById('producto-venta').value;
     let cant = document.getElementById('producto-cantidad').value;
     
     let data = {'nombreProducto':name, 'valorVenta':venta,
          'valorCompra':compra, 'cantidad':cant };
          
     let request = sendRequest('producto/','POST',data);
     request.onload = function(){
          window.location = 'Productos.html';
     };
     request.onerror = function(){
          alert('Error al guardar el registro');
     };
}

function updateProducto(){
     let id = document.getElementById('producto-id').value;
     let name = document.getElementById('producto-name').value;
     let compra = document.getElementById('producto-compra').value;
     let venta = document.getElementById('producto-venta').value;
     let cant = document.getElementById('producto-cantidad').value;
     
     let data = {'idProducto':id, 'nombreProducto':name, 'valorVenta':venta,
          'valorCompra':compra, 'cantidad':cant };
          
     let request = sendRequest('producto/list/'+id,'PUT',data);
     request.onload = function(){
          window.location = 'Productos.html';
     };
     request.onerror = function(){
          alert('Error al acctualizar el registro');
     };  
}

