
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


function loadData(){
     let request = sendRequest('ventaproducto/list','GET','');
     let table = document.getElementById('ventaproducto-table');
     table.innerHTML = "";
     request.onload = function(){
          let data = request.response;
          console.log(data);
          data.forEach((element)=>{
             table.innerHTML += `
               <tr class="table-warning">
                    <th>${element.idVenta}</th>
                    <td>${element.idTransaccion.idTransaccion}
                    ${element.idTransaccion.tipoTransaccion}</td>
                    <td>${element.idProducto.idProducto}
                    ${element.idProducto.nombreProducto}</td>
                    <th>${element.cantidadProducto}</th>
                    <th>${element.valorVenta}</th>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success"
                    onclick='window.location="FormVentaProducto.html?id=${element.idVenta}"' >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger"
                    onclick='deleteVenta(${element.idVenta})'>Eliminar</button>
                    </td>
               </tr>
               `;  
          });
     };
     
     request.onerror = function(){
       table.innerHTML = `
          <tr>
          <td colspan="7">Error al consultar la base de datos la tabla ventas</td>
          </tr>
          `;
     };
}

function loadVenta(idVenta){
     let request = sendRequest('ventaproducto/list/'+idVenta,'GET','');
     let id = document.getElementById('venta-id');
     let transaccionId = document.getElementById('TransaccionId');
     let productoId = document.getElementById('ProductoId');
     let cantidadProducto = document.getElementById('Cantidad');
     let valorVenta = document.getElementById('Valor');
     
     request.onload = function(){
          let data = request.response;
          console.log(data);
          id.value = data.idVenta;
          transaccionId.value = data.idTransaccion.idTransaccion;
          productoId.value = data.idProducto.idProducto;
          cantidadProducto.value = data.cantidadProducto;
          valorVenta.value = data.valorVenta;

     };
     
     request.onerror = function(){
          alert('Error al consultar los datos de una venta, revise el console log del navegador o el output del IDE tomcat');
     };
}

function deleteVenta(idVenta){
     let request = sendRequest('ventaproducto/list/'+idVenta,'DELETE','');
     request.onload = function(){
          loadData();
     };
}

function saveVentaProducto(){
     let transaccionId = document.getElementById('TransaccionId').value;
     let productoId = document.getElementById('ProductoId').value;
     let cantidadProducto = document.getElementById('Cantidad').value;
     let valorVenta = document.getElementById('Valor').value;
     
     let data ={
          'idTransaccion':{'idTransaccion':transaccionId},'idProducto':{'idProducto':productoId},
          'cantidadProducto':cantidadProducto ,'valorVenta':valorVenta         
     };
     
     let request = sendRequest('ventaproducto/','POST', data);
     request.onload = function (){
          window.location = 'VentasProducto.html';
     };
     request.onerror = function(){
          alert('Error al guardar la venta');
     }; 
}

function updateVentaProducto(){
     let id = document.getElementById('venta-id').value;
     let transaccionId = document.getElementById('TransaccionId').value;
     let productoId = document.getElementById('ProductoId').value;
     let cantidadProducto = document.getElementById('Cantidad').value;
     let valorVenta = document.getElementById('Valor').value;
     
     let data ={
          'idVenta':id,
          'idTransaccion':{'idTransaccion':transaccionId},'idProducto':{'idProducto':productoId},
          'cantidadProducto':cantidadProducto ,'valorVenta':valorVenta  
     };
     
     let request = sendRequest('ventaproducto/list/'+id,'PUT', data);
     request.onload = function (){
          window.location = 'VentasProducto.html';
     };
     request.onerror = function(){
          alert('Error al guardar la venta');
     }; 
}
