/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

function loadData(){
     let request = sendRequest('transaccion/list','GET','');
     let table = document.getElementById('transaccion-table');
     table.innerHTML = "";
     request.onload = function(){
          let data = request.response;
          console.log(data);
          data.forEach((element)=>{
             table.innerHTML += `
               <tr class="table-warning">
                    <th>${element.idTransaccion}</th>
                    <td>${element.tipoTransaccion}</td>
                    <td>${element.fechaTransaccion}</td>
                    <td>${element.idEmpleado.idEmpleado}
                    ${element.idEmpleado.nombre}</td>
                    <td>${element.idCiente.idCliente}
                    ${element.idCiente.nombre}</td>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success"
                    onclick='window.location="FormTransaccion.html?id=${element.idTransaccion}"' >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger"
                    onclick='deleteTransaccion(${element.idTransaccion})'>Eliminar</button>
                    </td>
               </tr>
               ` ;
          });
     };
     
     request.onerror = function(){
       table.innerHTML = `
          <tr>
          <td colspan="7">Error al consultar la base de datos la tabla transaccion</td>
          </tr>
          ` ; 
     };
}

function loadTransaccion(idTransaccion){
     let request = sendRequest('transaccion/list/'+idTransaccion,'GET','');
     let id = document.getElementById('transaccion-id');
     let tipoTransaccion = document.getElementById('tipo-transaccion');
     let fechaTransaccion = document.getElementById('fecha-transaccion');
     let idEmpleado = document.getElementById('empleadoId');
     let idCliente = document.getElementById('clienteId');
     
     request.onload = function(){
          let data = request.response;
          console.log(data);
          id.value = data.idTransaccion;
          tipoTransaccion.value = data.tipoTransaccion;
          fechaTransaccion.value = data.fechaTransaccion;
          idEmpleado.value = data.idEmpleado.idEmpleado;
          idCliente.value = data.idCiente.idCliente;
     };
     
     request.onerror = function(){
          alert('Error al consultar los datos de una transacción, revise el console log del navegador o el output del IDE tomcat');
     };
}

function deleteTransaccion(idTransaccion){
     let request = sendRequest('transaccion/list/'+idTransaccion,'DELETE','');
     request.onload = function(){
          loadData();
     };
}

function saveTransaccion(){
     let tipoTransaccion = document.getElementById('tipo-transaccion').value;
     let fechaTransaccion = document.getElementById('fecha-transaccion').value;
     let idEmpleado = document.getElementById('empleadoId').value;
     let idCliente = document.getElementById('clienteId').value;

     
     let data ={
          'tipoTransaccion':tipoTransaccion , 'fechaTransaccion':fechaTransaccion,
          'idEmpleado':{'idEmpleado':idEmpleado},'idCiente':{'idCliente':idCliente} 
     };
     
     let request = sendRequest('transaccion/','POST', data);
     request.onload = function (){
          window.location = 'Transacciones.html';
     };
     request.onerror = function(){
          alert('Error al guardar la transaccion');
     }; 
}

function updateTransaccion(){
     let id = document.getElementById('transaccion-id').value;
     let tipoTransaccion = document.getElementById('tipo-transaccion').value;
     let fechaTransaccion = document.getElementById('fecha-transaccion').value;
     let idEmpleado = document.getElementById('empleadoId').value;
     let idCliente = document.getElementById('clienteId').value;
     
     let data ={
          'idTransaccion':id,
          'tipoTransaccion':tipoTransaccion , 'fechaTransaccion':fechaTransaccion,
          'idEmpleado':{'idEmpleado':idEmpleado},'idCiente':{'idCliente':idCliente} 
     };
     
     let request = sendRequest('transaccion/list/'+id,'PUT', data);
     request.onload = function (){
          window.location = 'Transacciones.html';
     };
     request.onerror = function(){
          alert('Error al guardar la transacción');
     }; 
}