function loadData(){
     let request = sendRequest('cliente/list','GET','');
     let table = document.getElementById('clients-table');
     table.innerHTML="";
     request.onload= function (){
          let data = request.response;
          console.log(data);
          data.forEach((element,index)=> {
               //en las etiquetas <th>, td, despues del element. son los mismos nombres de las columnas de la base de datos, eje idProducto es como está en la tabla producto
             table.innerHTML += `
               <tr class="table-warning">
                    
                    <th>${element.idCliente}</th>
                    <td>${element.nombre}</td>
                    <td>${element.apellido}</td>
                    <td>${element.direccion}</td>
                    <td>${element.telefono}</td>
                    <td>${element.email}</td>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success" onclick="window.location='FormCliente.html?id=${element.idCliente}'" >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger" onclick='deleteCliente(${element.idCliente})'>Eliminar</button>
                    </td>
               </tr>
               `;
            
          });
     };
     request.onerror = function(){
          alert("Error al cargar la tabla Clientes");
     };
}

function loadCliente(idCliente){
     let request = sendRequest('cliente/list/'+idCliente,'GET','');
     let id = document.getElementById('cliente-id');
     let name = document.getElementById('cliente-name');
     let lName = document.getElementById('cliente-last-name');
     let direccion = document.getElementById('direccion');
     let phone = document.getElementById('phone');
     let email = document.getElementById("email");
     
     request.onload = function(){
          let data = request.response;
          //despues del igual lo que acompaña a data son las columnas de la tabla en la base de datos
          id.value= data.idCliente;
          name.value = data.nombre;
          lName.value = data.apellido;
          direccion.value= data.direccion;
          phone.value = data.telefono;
          email.value = data.email;
          
     };
     request.onerror = function(){
          alert("Error al consultar el cliente");
     };
     
}

function deleteCliente(idCliente){
     let request = sendRequest('cliente/list/'+idCliente,'DELETE','');
     request.onload = function(){
          loadData();
     };
}
 
function saveCliente(){
     let name = document.getElementById('cliente-name').value;
     let lName = document.getElementById('cliente-last-name').value;
     let direccion = document.getElementById('direccion').value;
     let phone = document.getElementById('phone').value;
     let email = document.getElementById('email').value;
     
    let data = {'nombre':name, 'apellido':lName,
          'direccion':direccion, 'telefono':phone,'email':email };
          
     let request = sendRequest('cliente/','POST',data);
     request.onload = function(){
          window.location = 'Clientes.html';
     };
     request.onerror = function(){
          alert('Error al guardar el registro');
     };
}

function updateCliente(){
     let id = document.getElementById('cliente-id').value;
     let name = document.getElementById('cliente-name').value;
     let lName = document.getElementById('cliente-last-name').value;
     let direccion = document.getElementById('direccion').value;
     let phone = document.getElementById('phone').value;
     let email = document.getElementById('email').value;
     
     let data = {'idCliente':id, 'nombre':name, 'apellido':lName,
          'direccion':direccion, 'telefono':phone,'email':email };
          
     let request = sendRequest('cliente/list/'+id,'PUT',data);
     request.onload = function(){
          window.location = 'Clientes.html';
     };
     request.onerror = function(){
          alert('Error al altualizar el registro');
     }; 
}

