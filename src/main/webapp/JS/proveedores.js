function loadData(){
     let request = sendRequest('proveedor/list','GET','');
     let table = document.getElementById('proveedores-table');
     table.innerHTML="";
     request.onload= function (){
          let data = request.response;
          console.log(data);
          data.forEach((element,index)=> {
               //en las etiquetas <th>, td, despues del element. son los mismos nombres de las columnas de la base de datos, eje idProducto es como está en la tabla producto
             table.innerHTML += `
               <tr class="table-warning">
                    
                    <th>${element.idProveedor}</th>
                    <td>${element.razonSocial}</td>
                    <td>${element.telefono}</td>
                    <td>${element.nit}</td>
                    <td>${element.direccion}</td>
                    <td>${element.email}</td>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success" onclick="window.location='FormProveedor.html?id=${element.idProveedor}'" >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger" onclick='deleteProveedor(${element.idProveedor})'>Eliminar</button>
                    </td>
               </tr>
               `;
            
          });
     };
     request.onerror = function(){
          table.innerHTML=`
          <tr>
               <td colspan="6">Error al recuperar los datos de transacción </td>
          </tr>`;
     };
}

function loadProveedor(idProveedor){
     let request = sendRequest('proveedor/list/'+idProveedor,'GET','');
     let id = document.getElementById('proveedor-id');
     let name = document.getElementById('proveedor-name');
     let phone = document.getElementById('phone');
     let nit = document.getElementById('NIT');
     let direccion = document.getElementById('direccion');
     let email = document.getElementById('email');
     
     request.onload = function(){
          let data = request.response;
          //despues del igual lo que acompaña a data son las columnas de la tabla en la base de datos
          id.value= data.idProveedor;
          name.value = data.razonSocial;
          phone.value = data.telefono;
          nit.value = data.nit;
          direccion.value= data.direccion;
          email.value = data.email;
          
          
     };
     request.onerror = function(){
          alert("Error al consultar el cliente");
     };
     
}

function deleteProveedor(idProveedor){
     let request = sendRequest('proveedor/list/'+idProveedor,'DELETE','');
     request.onload = function(){
          loadData();
     };
}
 
function saveProveedor(){
     let name = document.getElementById('proveedor-name').value;
     let phone = document.getElementById('phone').value;
     let nit = document.getElementById('NIT').value;
     let direccion = document.getElementById('direccion').value;
     let email = document.getElementById('email').value;
     
     
     
     let data = {'razonSocial':name,'telefono':phone,'nit':nit,
          'direccion':direccion,'email':email };
          
     let request = sendRequest('proveedor/','POST',data);
     request.onload = function(){
          window.location = 'Proveedores.html';
     };
     request.onerror = function(){
          alert('Error al guardar el registro');
     };
}

function updateProveedor(){
     let id = document.getElementById('proveedor-id').value;
     let name = document.getElementById('proveedor-name').value;
     let phone = document.getElementById('phone').value;
     let nit = document.getElementById('NIT').value;
     let direccion = document.getElementById('direccion').value;
     let email = document.getElementById('email').value;
     
     let data = {'idProveedor':id,'razonSocial':name,'telefono':phone,'nit':nit,
          'direccion':direccion,'email':email };
          
     let request = sendRequest('proveedor/list/'+id,'PUT',data);
     request.onload = function(){
          window.location = 'Proveedores.html';
     };
     request.onerror = function(){
          alert('Error al altualizar el registro');
     };
}

