function loadData(){
     let request = sendRequest('empleado/list','GET','');
     let table = document.getElementById('empleados-table');
     table.innerHTML="";
     request.onload= function (){
          let data = request.response;
          console.log(data);
          data.forEach((element,index)=> {
               //en las etiquetas <th>, td, despues del element. son los mismos nombres de las columnas de la base de datos, eje idProducto es como está en la tabla producto
             table.innerHTML += `
               <tr class="table-warning">
                    
                    <th>${element.idEmpleado}</th>
                    <td>${element.nombre}</td>
                    <td>${element.apellido}</td>
                    <td>${element.email}</td>
                    <td>${element.telefono}</td>
                    <td>${element.direccion}</td>
                    <td>${element.fechaEntrada}</td>
                    <td>
                         <button style="border-radius:20px;" type="button" class="btn btn-outline-success" onclick="window.location='FormEmpleado.html?id=${element.idEmpleado}'" >Editar</button>
                         <button style="border-radius:20px; " type="button" class="btn btn-outline-danger" onclick='deleteEmpleado(${element.idEmpleado})'>Eliminar</button>
                    </td>
               </tr>
               `;
            
          });
     };
     request.onerror = function(){
          alert("Error al cargar la tabla Empleados");
     };
}

function loadEmpleado(idEmpleado){
     let request = sendRequest('empleado/list/'+idEmpleado,'GET','');
     let id = document.getElementById('empleado-id');
     let name = document.getElementById('empleado-name');
     let lName = document.getElementById('empleado-last-name');
     let direccion = document.getElementById('direccion');
     let phone = document.getElementById('phone');
     let email = document.getElementById("email");
     let fEntrada = document.getElementById('fecha-entrada');
     
     request.onload = function(){
          let data = request.response;
          //despues del igual lo que acompaña a data son las columnas de la tabla en la base de datos
          id.value= data.idEmpleado;
          name.value = data.nombre;
          lName.value = data.apellido;
          email.value = data.email;
          phone.value = data.telefono;
          direccion.value= data.direccion;
          fEntrada.value = data.fechaEntrada;
          
          
     };
     request.onerror = function(){
          alert("Error al consultar el cliente");
     };
     
}

function deleteEmpleado(idEmpleado){
     let request = sendRequest('empleado/list/'+idEmpleado,'DELETE','');
     request.onload = function(){
          loadData();
     };
}

 
function saveEmpleado(){
     let name = document.getElementById('empleado-name').value;
     let lName = document.getElementById('empleado-last-name').value;
     let email = document.getElementById('email').value;
     let phone = document.getElementById('phone').value;
     let direccion = document.getElementById('direccion').value;
     let fEntrada = document.getElementById('fecha-entrada').value;
     
     let data = {'nombre':name, 'apellido':lName,'email':email,'telefono':phone,
          'direccion':direccion,'fechaEntrada':fEntrada };
          
     let request = sendRequest('empleado/','POST',data);
     request.onload = function(){
          window.location = 'Empleados.html';
     };
     request.onerror = function(){
          alert('Error al guardar el registro');
     };
}

function updateEmpleado(){
     let id = document.getElementById('empleado-id').value;
     let name = document.getElementById('empleado-name').value;
     let lName = document.getElementById('empleado-last-name').value;
     let email = document.getElementById('email').value;
     let phone = document.getElementById('phone').value;
     let direccion = document.getElementById('direccion').value;
     let fEntrada = document.getElementById('fecha-entrada').value;
     
     let data = {'idEmpleado':id, 'nombre':name, 'apellido':lName,'email':email,'telefono':phone,
          'direccion':direccion ,'fechaEntrada':fEntrada };
          
     let request = sendRequest('empleado/list/'+id,'PUT',data);
     request.onload = function(){
          window.location = 'Empleados.html';
     };
     request.onerror = function(){
          alert('Error al altualizar el registro');
     };  
}


