/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;

import com.ProyectoGrupo06.O55.interfaces.ClienteService;
import com.ProyectoGrupo06.O55.modelo.Cliente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/cliente")//nombre de la tabla en la DB
/**
 *
 * @crstian Abondano
 */
public class ClienteControlador {
    
        
    @Autowired
    private ClienteService clienteService;
    
    @PostMapping(value="/")
    public ResponseEntity<Cliente> agregar(@RequestBody Cliente cliente){
        Cliente obj=clienteService.save(cliente);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Cliente> eliminar (@PathVariable Integer id){ //para indicar el id
        Cliente obj=clienteService.findById(id);
        if (obj !=null){
            clienteService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Cliente> editar(@RequestBody Cliente cliente){
        Cliente obj=clienteService.findById(cliente.getIdCliente());
        if(obj!=null){
            
            obj.setNombre(cliente.getNombre());
            obj.setApellido(cliente.getApellido());
            obj.setDireccion(cliente.getDireccion());
            obj.setTelefono(cliente.getTelefono());
            obj.setEmail(cliente.getEmail());
            clienteService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<Cliente> consultarTodo(){
        return clienteService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public Cliente consultarPorId(@PathVariable Integer id){
        return clienteService.findById(id);
    }
}
