/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;

import com.ProyectoGrupo06.O55.interfaces.EmpleadoService;
import com.ProyectoGrupo06.O55.modelo.Empleado;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 *
 * @author david
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/empleado")//nombre de la tabla en la DB
public class EmpleadoControlador {
    @Autowired
    private EmpleadoService empleadoService;
    
    @PostMapping(value="/")
    public ResponseEntity<Empleado> agregar(@RequestBody Empleado empleado){
        Empleado obj = empleadoService.save(empleado);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Empleado> eliminar (@PathVariable Integer id){ //para indicar el id
        Empleado obj = empleadoService.findById(id);
        if (obj !=null){
            empleadoService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Empleado> editar(@RequestBody Empleado empleado){
        Empleado obj = empleadoService.findById(empleado.getIdEmpleado());
        if(obj!=null){
            
            obj.setNombre(empleado.getNombre());
            obj.setApellido(empleado.getApellido());
            obj.setEmail(empleado.getEmail());
            obj.setTelefono(empleado.getTelefono());
            obj.setDireccion(empleado.getDireccion());
            obj.setFechaEntrada(empleado.getFechaEntrada());
            empleadoService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<Empleado> consultarTodo(){
        return empleadoService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public Empleado consultarPorId(@PathVariable Integer id){
        return empleadoService.findById(id);
    }
    
}
