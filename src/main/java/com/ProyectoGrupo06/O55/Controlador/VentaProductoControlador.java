/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;

import com.ProyectoGrupo06.O55.interfaces.VentaService;
import com.ProyectoGrupo06.O55.modelo.VentaProducto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author david
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/ventaproducto")//nombre de la tabla en la DB
public class VentaProductoControlador {
    @Autowired
    private VentaService ventaService;
    
    @PostMapping(value="/")
    public ResponseEntity<VentaProducto> agregar(@RequestBody VentaProducto ventaProducto){
        VentaProducto obj = ventaService.save(ventaProducto);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<VentaProducto> eliminar (@PathVariable Integer id){ //para indicar el id
        VentaProducto obj = ventaService.findById(id);
        if (obj !=null){
            ventaService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<VentaProducto> editar(@RequestBody VentaProducto ventaProducto){
        VentaProducto obj = ventaService.findById(ventaProducto.getIdVenta());
        if(obj!=null){
            
            obj.setIdTransaccion(ventaProducto.getIdTransaccion());
            obj.setIdProducto(ventaProducto.getIdProducto());
            obj.setCantidadProducto(ventaProducto.getCantidadProducto());
            obj.setValorVenta(ventaProducto.getValorVenta());
            ventaService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<VentaProducto> consultarTodo(){
        return ventaService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public VentaProducto consultarPorId(@PathVariable Integer id){
        return ventaService.findById(id);
    }
}
