/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;

import com.ProyectoGrupo06.O55.interfaces.CompraService;
import com.ProyectoGrupo06.O55.modelo.CompraProducto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nelson Alvarez
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/compraproducto")//nombre de la tabla en la DB
public class CompraControlador {
    
    @Autowired
    private CompraService compraService;
    
    @PostMapping(value="/")
    public ResponseEntity<CompraProducto> agregar(@RequestBody CompraProducto compraProducto){
        CompraProducto obj=compraService.save(compraProducto);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<CompraProducto> eliminar (@PathVariable Integer id){ //para indicar el id
        CompraProducto obj=compraService.findById(id);
        if (obj !=null){
            compraService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<CompraProducto> editar(@RequestBody CompraProducto compraProducto){
        CompraProducto obj=compraService.findById(compraProducto.getIdCompra());
        if(obj!=null){
            
            obj.setIdProducto(compraProducto.getIdProducto());
            obj.setIdProveedor(compraProducto.getIdProveedor());
            obj.setCantidadProducto(compraProducto.getCantidadProducto());
            obj.setValorCompra(compraProducto.getValorCompra());
            compraService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<CompraProducto> consultarTodo(){
        return compraService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public CompraProducto consultarPorId(@PathVariable Integer id){
        return compraService.findById(id);
    }
    
}
