/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;

import com.ProyectoGrupo06.O55.interfaces.TransaccionService;
import com.ProyectoGrupo06.O55.modelo.Transaccion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author david
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/transaccion")//nombre de la tabla en la DB
public class TransaccionControlador {
    @Autowired
    private TransaccionService transaccionService;
    
    @PostMapping(value="/")
    public ResponseEntity<Transaccion> agregar(@RequestBody Transaccion transaccion){
        Transaccion obj = transaccionService.save(transaccion);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Transaccion> eliminar (@PathVariable Integer id){ //para indicar el id
        Transaccion obj = transaccionService.findById(id);
        if (obj !=null){
            transaccionService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Transaccion> editar(@RequestBody Transaccion transaccion){
        Transaccion obj = transaccionService.findById(transaccion.getIdTransaccion());
        if(obj!=null){
            obj.setTipoTransaccion(transaccion.getTipoTransaccion());
            obj.setFechaTransaccion(transaccion.getFechaTransaccion());
            obj.setIdEmpleado(transaccion.getIdEmpleado());
            obj.setIdCiente(transaccion.getIdCiente());
            transaccionService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<Transaccion> consultarTodo(){
        return transaccionService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public Transaccion consultarPorId(@PathVariable Integer id){
        return transaccionService.findById(id);
    }
    
}
