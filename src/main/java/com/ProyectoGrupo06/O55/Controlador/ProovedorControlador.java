/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;

import com.ProyectoGrupo06.O55.interfaces.ProveedorService;
import com.ProyectoGrupo06.O55.modelo.Proveedor;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
/**
 *
 * @author david
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/proveedor")//nombre de la tabla en la DB
public class ProovedorControlador {

    @Autowired
    private ProveedorService proveedorService;
    
    @PostMapping(value="/")
    public ResponseEntity<Proveedor> agregar(@RequestBody Proveedor proveedor){
        Proveedor obj = proveedorService.save(proveedor);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Proveedor> eliminar (@PathVariable Integer id){ //para indicar el id
        Proveedor obj = proveedorService.findById(id);
        if (obj !=null){
            proveedorService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Proveedor> editar(@RequestBody Proveedor proveedor){
        Proveedor obj = proveedorService.findById(proveedor.getIdProveedor());
        if(obj!=null){
            
            obj.setRazonSocial(proveedor.getRazonSocial());
            obj.setTelefono(proveedor.getTelefono());
            obj.setNit(proveedor.getNit());
            obj.setDireccion(proveedor.getDireccion());
            obj.setEmail(proveedor.getEmail());
            proveedorService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<Proveedor> consultarTodo(){
        return proveedorService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public Proveedor consultarPorId(@PathVariable Integer id){
        return proveedorService.findById(id);
    }
    
}
