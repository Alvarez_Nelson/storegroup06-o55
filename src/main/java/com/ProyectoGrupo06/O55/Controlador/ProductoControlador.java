/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Controlador;


import com.ProyectoGrupo06.O55.interfaces.ProductoService;
import com.ProyectoGrupo06.O55.modelo.Producto;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nelson Alvarez
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/producto")//nombre de la tabla en la DB
public class ProductoControlador {
    
    @Autowired
    private ProductoService productoService;
    
    @PostMapping(value="/")
    public ResponseEntity<Producto> agregar(@RequestBody Producto producto){
        Producto obj = productoService.save(producto);
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @DeleteMapping(value="/list/{id}")
    public ResponseEntity<Producto> eliminar (@PathVariable Integer id){ //para indicar el id
        Producto obj=productoService.findById(id);
        if (obj !=null){
            productoService.delete(id);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj,HttpStatus.OK);
    }
    
    @PutMapping(value="/list/{id}")
    public ResponseEntity<Producto> editar(@RequestBody Producto producto){
        Producto obj=productoService.findById(producto.getIdProducto());
        if(obj!=null){
            obj.setNombreProducto(producto.getNombreProducto());
            obj.setValorCompra(producto.getValorCompra());
            obj.setValorVenta(producto.getValorVenta());
            obj.setCantidad(producto.getCantidad());
            productoService.save(obj);
            return new ResponseEntity<>(obj,HttpStatus.OK);
        }else{
            return new ResponseEntity<>(obj,HttpStatus.INTERNAL_SERVER_ERROR);
        }
        
    }
    
    @GetMapping(value="/list")
    public List<Producto> consultarTodo(){
        return productoService.findAll();
    }
    
    @GetMapping(value="/list/{id}")
    public Producto consultarPorId(@PathVariable Integer id){
        return productoService.findById(id);
    }
}

