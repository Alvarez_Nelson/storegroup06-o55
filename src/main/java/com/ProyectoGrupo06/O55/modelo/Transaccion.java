/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author CARLOS
 */
@Entity
@Table(name="transaccion")

public class Transaccion implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idTransaccion")
    private Integer idTransaccion;
    
    @Column(name="tipoTransaccion")
    private String tipoTransaccion;
    
    @Column(name="fechaTransaccion")
    private String fechaTransaccion;
    
    @ManyToOne
    @JoinColumn(name = "idEmpleado")
    private Empleado idEmpleado;
    
    @ManyToOne
    @JoinColumn(name = "idCliente")
    private Cliente idCiente;

    public Transaccion() {
    }

    public Transaccion(Integer idTransaccion, String tipoTransaccion, String fechaTransaccion, Empleado idEmpleado, Cliente idCiente) {
        this.idTransaccion = idTransaccion;
        this.tipoTransaccion = tipoTransaccion;
        this.fechaTransaccion = fechaTransaccion;
        this.idEmpleado = idEmpleado;
        this.idCiente = idCiente;
    }

    /**
     * @return the idTransaccion
     */
    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * @param idTransaccion the idTransaccion to set
     */
    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    /**
     * @return the tipoTransaccion
     */
    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    /**
     * @param tipoTransaccion the tipoTransaccion to set
     */
    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    /**
     * @return the fechaTransaccion
     */
    public String getFechaTransaccion() {
        return fechaTransaccion;
    }

    /**
     * @param fechaTransaccion the fechaTransaccion to set
     */
    public void setFechaTransaccion(String fechaTransaccion) {
        this.fechaTransaccion = fechaTransaccion;
    }

    /**
     * @return the idEmpleado
     */
    public Empleado getIdEmpleado() {
        return idEmpleado;
    }

    /**
     * @param idEmpleado the idEmpleado to set
     */
    public void setIdEmpleado(Empleado idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    /**
     * @return the idCiente
     */
    public Cliente getIdCiente() {
        return idCiente;
    }

    /**
     * @param idCiente the idCiente to set
     */
    public void setIdCiente(Cliente idCiente) {
        this.idCiente = idCiente;
    }
    

    
}
