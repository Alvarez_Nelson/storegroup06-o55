/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nelson Alvarez
 */
@Entity
@Table(name="ventaproducto")
public class VentaProducto implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idVenta")
    private Integer idVenta;
    
    @ManyToOne
    @JoinColumn(name="idTransaccion")
    private Transaccion idTransaccion;
    
    @ManyToOne
    @JoinColumn(name="IdProducto")
    private Producto idProducto;
    
    @Column(name="cantidadProducto")
    private Integer cantidadProducto;
    
    @Column(name="valorVenta")
    private double valorVenta;

    public VentaProducto() {
    }

    public VentaProducto(Integer idVenta, Transaccion idTransaccion, Producto idProducto, Integer cantidadProducto, double valorVenta) {
        this.idVenta = idVenta;
        this.idTransaccion = idTransaccion;
        this.idProducto = idProducto;
        this.cantidadProducto = cantidadProducto;
        this.valorVenta = valorVenta;
    }

    public Integer getIdVenta() {
        return idVenta;
    }

    public void setIdVenta(Integer idVenta) {
        this.idVenta = idVenta;
    }

    public Transaccion getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(Transaccion idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Producto getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getCantidadProducto() {
        return cantidadProducto;
    }

    public void setCantidadProducto(Integer cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    public double getValorVenta() {
        return valorVenta;
    }

    public void setValorVenta(double valorVenta) {
        this.valorVenta = valorVenta;
    }
    
    
}
