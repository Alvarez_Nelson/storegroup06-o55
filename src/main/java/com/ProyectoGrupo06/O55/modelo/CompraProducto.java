/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.modelo;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Nelson Alvarez
 */
@Entity
@Table(name="compraproducto")
public class CompraProducto implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idCompra")
    private Integer idCompra;
    
    @ManyToOne
    @JoinColumn(name="idProducto")
    private Producto idProducto;
    
    @ManyToOne
    @JoinColumn(name="idProveedor")
    private Proveedor idProveedor;
    
    @Column(name="cantidadProducto")
    private Integer cantidadProducto;
    
    @Column(name="valorCompra")
    private double valorCompra;

    public CompraProducto() {
    }

    public CompraProducto(Integer idCompra, Producto idProducto, Proveedor idProveedor, Integer cantidadProducto, double valorCompra) {
        this.idCompra = idCompra;
        this.idProducto = idProducto;
        this.idProveedor = idProveedor;
        this.cantidadProducto = cantidadProducto;
        this.valorCompra = valorCompra;
    }

    /**
     * @return the idCompra
     */
    public Integer getIdCompra() {
        return idCompra;
    }

    /**
     * @param idCompra the idCompra to set
     */
    public void setIdCompra(Integer idCompra) {
        this.idCompra = idCompra;
    }

    /**
     * @return the idProducto
     */
    public Producto getIdProducto() {
        return idProducto;
    }

    /**
     * @param idProducto the idProducto to set
     */
    public void setIdProducto(Producto idProducto) {
        this.idProducto = idProducto;
    }

    /**
     * @return the idProveedor
     */
    public Proveedor getIdProveedor() {
        return idProveedor;
    }

    /**
     * @param idProveedor the idProveedor to set
     */
    public void setIdProveedor(Proveedor idProveedor) {
        this.idProveedor = idProveedor;
    }

    /**
     * @return the cantidadProducto
     */
    public Integer getCantidadProducto() {
        return cantidadProducto;
    }

    /**
     * @param cantidadProducto the cantidadProducto to set
     */
    public void setCantidadProducto(Integer cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }

    /**
     * @return the valorCompra
     */
    public double getValorCompra() {
        return valorCompra;
    }

    /**
     * @param valorCompra the valorCompra to set
     */
    public void setValorCompra(double valorCompra) {
        this.valorCompra = valorCompra;
    }
    
    
    
}
