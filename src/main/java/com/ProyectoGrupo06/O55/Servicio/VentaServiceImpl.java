/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.VentaService;
import com.ProyectoGrupo06.O55.modelo.VentaProducto;
import com.ProyectoGrupo06.O55.servicios.repositorios.VentaProductoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class VentaServiceImpl implements VentaService{
    @Autowired
    private VentaProductoDAO ventaProductoDAO;
    
    @Override
    @Transactional(readOnly=false)
    public VentaProducto save(VentaProducto ventaProducto){
        return ventaProductoDAO.save(ventaProducto);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        ventaProductoDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public VentaProducto findById(Integer id){
        return ventaProductoDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<VentaProducto> findAll(){
        return (List<VentaProducto>) ventaProductoDAO.findAll();
    }
}
