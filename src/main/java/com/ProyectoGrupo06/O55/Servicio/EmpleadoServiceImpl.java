/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.EmpleadoService;
import com.ProyectoGrupo06.O55.modelo.Empleado;
import com.ProyectoGrupo06.O55.servicios.repositorios.EmpleadoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class EmpleadoServiceImpl implements EmpleadoService{
    
    @Autowired
    private EmpleadoDAO empleadoDAO;
    
    @Override
    @Transactional(readOnly=false)
    public Empleado save(Empleado empleado){
        return empleadoDAO.save(empleado);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        empleadoDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Empleado findById(Integer id){
        return empleadoDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Empleado> findAll(){
        return (List<Empleado>) empleadoDAO.findAll();
    }
}
