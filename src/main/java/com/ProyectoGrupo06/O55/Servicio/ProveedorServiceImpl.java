/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.ProveedorService;
import com.ProyectoGrupo06.O55.modelo.Proveedor;
import com.ProyectoGrupo06.O55.servicios.repositorios.ProveedorDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class ProveedorServiceImpl implements ProveedorService{
    @Autowired
    private ProveedorDAO proveedorDAO;
    
    @Override
    @Transactional(readOnly=false)
    public Proveedor save(Proveedor proveedor){
        return proveedorDAO.save(proveedor);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        proveedorDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Proveedor findById(Integer id){
        return proveedorDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Proveedor> findAll(){
        return (List<Proveedor>) proveedorDAO.findAll();
    }
}
