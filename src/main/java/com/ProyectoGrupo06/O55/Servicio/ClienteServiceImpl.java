/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.ClienteService;
import com.ProyectoGrupo06.O55.modelo.Cliente;
import com.ProyectoGrupo06.O55.servicios.repositorios.ClienteDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class ClienteServiceImpl implements ClienteService{
    @Autowired
    private ClienteDAO clienteDAO;
    
    @Override
    @Transactional(readOnly=false)
    public Cliente save(Cliente cliente){
        return clienteDAO.save(cliente);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        clienteDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Cliente findById(Integer id){
        return clienteDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Cliente> findAll(){
        return (List<Cliente>) clienteDAO.findAll();
    }
}
