/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.TransaccionService;
import com.ProyectoGrupo06.O55.modelo.Transaccion;
import com.ProyectoGrupo06.O55.servicios.repositorios.TransaccionDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class TransaccionServiceImpl implements TransaccionService{
    @Autowired
    private TransaccionDAO transaccionDAO;
    
    @Override
    @Transactional(readOnly=false)
    public Transaccion save(Transaccion transaccion){
        return transaccionDAO.save(transaccion);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        transaccionDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Transaccion findById(Integer id){
        return transaccionDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Transaccion> findAll(){
        return (List<Transaccion>) transaccionDAO.findAll();
    }
}
