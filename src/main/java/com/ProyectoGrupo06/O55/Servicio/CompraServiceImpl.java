/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.CompraService;
import com.ProyectoGrupo06.O55.modelo.CompraProducto;
import com.ProyectoGrupo06.O55.servicios.repositorios.CompraProductoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class CompraServiceImpl implements CompraService{
    
    @Autowired
    private CompraProductoDAO compraProductoDAO;
    
    @Override
    @Transactional(readOnly=false)
    public CompraProducto save(CompraProducto compraProducto){
        return compraProductoDAO.save(compraProducto);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        compraProductoDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public CompraProducto findById(Integer id){
        return compraProductoDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<CompraProducto> findAll(){
        return (List<CompraProducto>) compraProductoDAO.findAll();
    }
}
