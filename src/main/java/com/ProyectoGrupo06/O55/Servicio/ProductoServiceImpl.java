/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.ProyectoGrupo06.O55.Servicio;

import com.ProyectoGrupo06.O55.interfaces.ProductoService;
import com.ProyectoGrupo06.O55.modelo.Producto;
import com.ProyectoGrupo06.O55.servicios.repositorios.ProductoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nelson Alvarez
 */
@Service
public class ProductoServiceImpl implements ProductoService{
    
    @Autowired
    private ProductoDAO productoDAO;
    
    @Override
    @Transactional(readOnly=false)
    public Producto save(Producto producto){
        return productoDAO.save(producto);
    }
    
    @Override
    @Transactional(readOnly=false)
    public void delete(Integer id){
        productoDAO.deleteById(id);
    }
    
    @Override
    @Transactional(readOnly=true)
    public Producto findById(Integer id){
        return productoDAO.findById(id).orElse(null);
    }
    
    @Override
    @Transactional(readOnly=true)
    public List<Producto> findAll(){
        return (List<Producto>) productoDAO.findAll();
    }
    
}
