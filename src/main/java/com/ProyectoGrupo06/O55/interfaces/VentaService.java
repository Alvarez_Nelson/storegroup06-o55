/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.ProyectoGrupo06.O55.interfaces;

import com.ProyectoGrupo06.O55.modelo.VentaProducto;
import java.util.List;

/**
 *
 * @author Nelson Alvarez
 */
public interface VentaService {
    public VentaProducto save(VentaProducto ventaProducto);
    public void delete(Integer id);
    public VentaProducto findById(Integer id);
    public List<VentaProducto> findAll();
}
