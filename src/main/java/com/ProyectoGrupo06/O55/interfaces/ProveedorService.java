/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.ProyectoGrupo06.O55.interfaces;

import com.ProyectoGrupo06.O55.modelo.Proveedor;
import java.util.List;

/**
 *
 * @author Nelson Alvarez
 */
public interface ProveedorService {
    public Proveedor save(Proveedor proveedor);
    public void delete(Integer id);
    public Proveedor findById(Integer id);
    public List<Proveedor> findAll();
}
