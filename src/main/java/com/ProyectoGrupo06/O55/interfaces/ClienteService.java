/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.ProyectoGrupo06.O55.interfaces;

import com.ProyectoGrupo06.O55.modelo.Cliente;
import java.util.List;

/**
 *
 * @author Nelson Alvarez
 */
public interface ClienteService {
    public Cliente save(Cliente cliente);
    public void delete(Integer id);
    public Cliente findById(Integer id);
    public List<Cliente> findAll();
}
