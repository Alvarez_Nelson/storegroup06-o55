/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.ProyectoGrupo06.O55.interfaces;

import com.ProyectoGrupo06.O55.modelo.CompraProducto;
import java.util.List;

/**
 *
 * @author Nelson Alvarez
 */
public interface CompraService {
    public CompraProducto save(CompraProducto compraProducto);
    public void delete(Integer id);
    public CompraProducto findById(Integer id);
    public List<CompraProducto> findAll();
}
