/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.ProyectoGrupo06.O55.interfaces;

import com.ProyectoGrupo06.O55.modelo.Empleado;
import java.util.List;

/**
 *
 * @author Nelson Alvarez
 */
public interface EmpleadoService {
    public Empleado save(Empleado empleado);
    public void delete(Integer id);
    public Empleado findById(Integer id);
    public List<Empleado> findAll();
}
