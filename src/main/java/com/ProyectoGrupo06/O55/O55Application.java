package com.ProyectoGrupo06.O55;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class O55Application {

	public static void main(String[] args) {
		SpringApplication.run(O55Application.class, args);
	}

}
