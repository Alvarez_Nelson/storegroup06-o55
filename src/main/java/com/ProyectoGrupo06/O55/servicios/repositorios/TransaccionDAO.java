/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ProyectoGrupo06.O55.servicios.repositorios;
import org.springframework.data.repository.CrudRepository;
import com.ProyectoGrupo06.O55.modelo.Transaccion;

/**
 *
 * @author USUARIO
 */
public interface TransaccionDAO extends CrudRepository<Transaccion, Integer> {
    
}
