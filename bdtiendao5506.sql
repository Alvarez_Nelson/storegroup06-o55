-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-09-2022 a las 03:01:56
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdtiendao5506`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `apellido`, `direccion`, `telefono`, `email`) VALUES
(1, 'Sanson', 'Binch', '03791 Arapahoe Court', '602-940-5163', 'sbinch0@aol.com'),
(2, 'Maximilian', 'Teall', '071 Sunnyside Point', '443-848-2228', 'mteall1@ibm.com'),
(3, 'Ivan', 'Gathercole', '1 Holmberg Lane', '897-465-2975', 'egathercole2@youtu.be'),
(4, 'Tine', 'Tsar', '6 Pierstorff Center', '843-607-3831', 'ttsar3@google.fr'),
(5, 'Odille', 'Blezard', '39 Continental Center', '496-516-6623', 'oblezard4@ameblo.jp'),
(6, 'Ulla', 'Levane', '55 Butterfield Road', '583-941-8245', 'ulevane5@mapy.cz'),
(7, 'Armando', 'Lesor', '63 Ridge Oak Center', '617-520-5354', 'alesor6@google.es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compraproducto`
--

CREATE TABLE `compraproducto` (
  `idCompra` int(11) UNSIGNED NOT NULL,
  `idProducto` int(11) UNSIGNED NOT NULL,
  `idProveedor` int(11) UNSIGNED NOT NULL,
  `cantidadProducto` int(10) UNSIGNED NOT NULL,
  `valorCompra` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `compraproducto`
--

INSERT INTO `compraproducto` (`idCompra`, `idProducto`, `idProveedor`, `cantidadProducto`, `valorCompra`) VALUES
(1, 1, 4, 58, 44011),
(2, 6, 3, 36, 60969),
(3, 3, 8, 19, 78473),
(4, 10, 6, 55, 50208),
(5, 3, 4, 23, 72485),
(7, 9, 3, 53, 68491),
(8, 3, 5, 18, 53439),
(9, 10, 8, 13, 52760),
(10, 6, 2, 19, 13081),
(11, 1, 8, 27, 22417),
(12, 3, 7, 52, 11210),
(13, 8, 9, 13, 76183),
(14, 5, 6, 49, 11711),
(15, 7, 3, 44, 64185),
(16, 5, 3, 26, 55600),
(17, 8, 2, 21, 16430),
(18, 3, 6, 34, 71957),
(19, 1, 2, 17, 9718),
(20, 8, 9, 28, 23583),
(21, 5, 4, 51, 20785),
(22, 7, 5, 38, 77849),
(23, 1, 9, 46, 18735),
(24, 7, 4, 20, 71147);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(11) UNSIGNED NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fechaEntrada` varchar(16) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `nombre`, `apellido`, `email`, `telefono`, `direccion`, `fechaEntrada`) VALUES
(2, 'Adriena', 'Pearne', 'apearne0@privacy.gov.au', '+86 255 203 4066', '49 Fairview Trail', '24/08/2020'),
(3, 'Maura', 'Skentelbery', 'mskentelberyk@phpbb.com', '+86 300 224 0895', '88624 Ridgeway Street', '08/01/2021'),
(4, 'Lena', 'Bridgens', 'lbridgensj@wired.com', '+51 859 726 1625', '4 Monica Lane', '19/01/2021'),
(5, 'Isidoro', 'Lamp', 'ilampi@storify.com', '+62 745 434 5190', '91752 Stang Point', '25/01/2022'),
(6, 'Caroljean', 'Addyman', 'caddymanh@newyorker.com', '+86 152 275 5440', '66534 Truax Terrace', '30/08/2021'),
(7, 'Gibbie', 'Shellsheere', 'gshellsheereg@baidu.com', '+351 651 448 1455', '77075 Northwestern Point', '31/08/2022'),
(8, 'Charmian', 'Malimoe', 'cmalimoef@nasa.gov', '+506 105 491 0461', '4665 Fieldstone Road', '04/05/2022'),
(9, 'Merissa', 'Ixor', 'mixore@cam.ac.uk', '+86 212 272 5847', '06 Melody Crossing', '27/01/2022'),
(10, 'Darsey', 'Sprasen', 'dsprasend@yandex.ru', '+57 209 203 0924', '532 Meadow Vale Terrace', '13/03/2022'),
(12, 'Ashly', 'Huddy', 'ahuddyb@pcworld.com', '+420 846 886 4221', '4 Mifflin Lane', '22/11/2020'),
(13, 'Elsey', 'Seviour', 'esevioura@plala.or.jp', '+351 586 760 3763', '3 Corben Place', '29/11/2021'),
(14, 'Jaquith', 'Oxby', 'joxby9@squarespace.com', '+62 868 821 4972', '265 Bluejay Center', '02/10/2020'),
(15, 'Rancell', 'Codeman', 'rcodeman8@mayoclinic.com', '+62 177 249 1856', '0 Lake View Center', '26/12/2020'),
(16, 'Mercedes', 'Kuhlmey', 'mkuhlmey7@ucla.edu', '+62 593 607 2878', '827 Chinook Place', '22/04/2021'),
(17, 'Kaja', 'Hawse', 'khawse6@va.gov', '+355 522 213 0304', '287 Tony Junction', '20/05/2021'),
(18, 'Garfield', 'Carnalan', 'gcarnalan5@icio.us', '+63 950 742 9472', '84904 Mcguire Place', '19/09/2020');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) UNSIGNED NOT NULL,
  `nombreProducto` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `valorCompra` double NOT NULL,
  `valorVenta` double NOT NULL,
  `cantidad` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `nombreProducto`, `valorCompra`, `valorVenta`, `cantidad`) VALUES
(1, 'Platano', 4000, 5000, 4),
(2, 'Cafe', 3000, 4500, 20),
(3, 'Huevos', 5000, 7000, 120),
(5, 'Leche', 4500, 6000, 120),
(6, 'Tray - 12in Rnd Blk', 3361, 9928, 80),
(7, 'Pork Ham Prager', 6220, 7400, 37),
(8, 'Wine - White, Colubia Cresh', 6800, 8900, 21),
(9, 'Vinegar - Champagne', 14000, 25000, 20),
(10, 'Leche', 1000, 2000, 1000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) UNSIGNED NOT NULL,
  `razonSocial` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `nit` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `razonSocial`, `telefono`, `nit`, `direccion`, `email`) VALUES
(1, 'Jazzy', '+237 912 648 1662', '88.13.31.236', '9 Little Fleur Way', 'zlief0@gravatar.com'),
(2, 'Chatterbridge', '+86 321 928 3378', '85.249.91.231', '5 Mcbride Parkway', 'iguiet1@bigcartel.com'),
(3, 'Manzanaton', '+1 619 494 2988', '28.136.117.46', '05 Rutledge Plaza', 'hlong2@wsj.com'),
(4, 'Wikibox', '+46 179 712 2379', '178.185.165.194', '854 Oak Place', 'dlyddon4@drupal.org'),
(5, 'Talane', '+54 466 990 8310', '178.9.66.239', '8 Arkansas Park', 'tdiamant6@infoseek.co.jp'),
(6, 'Devify', '+66 428 861 8305', '66.68.61.185', '2402 Westerfield Street', 'pemanuele9@forbes.com'),
(7, 'Quinu', '+381 236 410 0915', '220.227.114.126', '54 Waywood Circle', 'challedeb@people.com.cn'),
(8, 'Youbridge', '+7 651 122 3099', '9.206.192.127', '913 Kenwood Plaza', 'dguntonc@sourceforge.net'),
(9, 'Jaxnation', '+63 999 859 0443', '18.238.47.65', '244 Shasta Circle', 'tbanthorped@flickr.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaccion`
--

CREATE TABLE `transaccion` (
  `idTransaccion` int(11) UNSIGNED NOT NULL,
  `tipoTransaccion` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `fechaTransaccion` varchar(16) COLLATE utf8_spanish_ci NOT NULL,
  `idEmpleado` int(11) UNSIGNED NOT NULL,
  `idCliente` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `transaccion`
--

INSERT INTO `transaccion` (`idTransaccion`, `tipoTransaccion`, `fechaTransaccion`, `idEmpleado`, `idCliente`) VALUES
(2, 'venta', '05/11/2021', 13, 1),
(3, 'devolucion', '18/04/2022', 15, 2),
(4, 'devolucion', '20/09/2021', 3, 5),
(6, 'venta', '01/11/2021', 6, 6),
(7, 'devolucion', '02/09/2021', 9, 3),
(9, 'devolucion', '29/05/2022', 5, 1),
(10, 'devolucion', '17/02/2022', 18, 3),
(11, 'venta', '28/12/2021', 9, 4),
(12, 'devolucion', '21/06/2021', 4, 7),
(13, 'venta', '25/11/2021', 9, 6),
(14, 'venta', '14/09/2021', 15, 7),
(15, 'devolucion', '20/08/2021', 6, 1),
(16, 'Devolucion', '27/04/2021', 10, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventaproducto`
--

CREATE TABLE `ventaproducto` (
  `idVenta` int(10) UNSIGNED NOT NULL,
  `idTransaccion` int(11) UNSIGNED NOT NULL,
  `IdProducto` int(11) UNSIGNED NOT NULL,
  `cantidadProducto` int(20) UNSIGNED NOT NULL,
  `valorVenta` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ventaproducto`
--

INSERT INTO `ventaproducto` (`idVenta`, `idTransaccion`, `IdProducto`, `cantidadProducto`, `valorVenta`) VALUES
(1, 11, 3, 20, 40000),
(2, 13, 1, 60, 60000),
(3, 11, 2, 50, 70000),
(5, 12, 5, 15, 15000);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `compraproducto`
--
ALTER TABLE `compraproducto`
  ADD PRIMARY KEY (`idCompra`),
  ADD KEY `idProducto` (`idProducto`),
  ADD KEY `idProveedor` (`idProveedor`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`idTransaccion`),
  ADD KEY `idCliente` (`idCliente`),
  ADD KEY `idEmpleado` (`idEmpleado`);

--
-- Indices de la tabla `ventaproducto`
--
ALTER TABLE `ventaproducto`
  ADD PRIMARY KEY (`idVenta`),
  ADD KEY `idTransaccion` (`idTransaccion`),
  ADD KEY `IdProducto` (`IdProducto`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `compraproducto`
--
ALTER TABLE `compraproducto`
  MODIFY `idCompra` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `idTransaccion` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `ventaproducto`
--
ALTER TABLE `ventaproducto`
  MODIFY `idVenta` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compraproducto`
--
ALTER TABLE `compraproducto`
  ADD CONSTRAINT `compraproducto_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `compraproducto_ibfk_2` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `transaccion_ibfk_1` FOREIGN KEY (`idEmpleado`) REFERENCES `empleado` (`idEmpleado`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaccion_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ventaproducto`
--
ALTER TABLE `ventaproducto`
  ADD CONSTRAINT `ventaproducto_ibfk_1` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`idProducto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ventaproducto_ibfk_2` FOREIGN KEY (`idTransaccion`) REFERENCES `transaccion` (`idTransaccion`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
